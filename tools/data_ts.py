#-*- coding: utf-8 -*-
# download data from tushare (need 'token.tmp' file having paid-token inside)
# 20220213 Wanjo
# usage: python -i data_ts.py 000001.SZ 2021

from mypy import load,argv,argc,touch_dir,time_maker

config_o = load('../tmp/config.json')
assert config_o, 'Please check ../tmp/config.json'
#print(config_o)

freq = '1min'
root_data_path = config_o[f'root_data_path_{freq}']

code = argv[1]
yyyy = argv[2]

tgt_dir = f'{root_data_path}/ts{yyyy}'
tgt_fn = f'{tgt_dir}/{code}.parquet'
print(f'downloading for {tgt_fn}...')

from mytushare import biz

yymm_base = 100 * int(yyyy[2:4])

dfa = []
for m in range(1,13):
    yymm = yymm_base + m
    #df = biz.get_df_yymm(code, yymm)
    df = biz.get_df_yymm(code, yymm, save=False)
    df['volume'] = df.vol.astype('int64')
    df['amount'] = df.amount.astype('float64')
    df.index=1000* df['trade_time'].map(lambda v:time_maker(date=v,infmt='%Y-%m-%d %H:%M:%S', outfmt=''))
    dfx = df[['open','high','low','close','volume','amount']] # OHLCVA
    dfa.append(dfx.sort_index())

import pandas as pd
dfo = pd.concat(dfa)
print(len(dfo))
#dfo = dfo[~dfo.index.duplicated()]
#print(len(dfo))

touch_dir(tgt_dir)
dfo.to_parquet(tgt_fn)

# check
dfcheck = pd.read_parquet(tgt_fn)
dfcheck['tt'] = pd.DatetimeIndex((dfcheck.index+8*3600000)*1000000)
print(dfcheck)

# TODO load back and save len to audit.h5
