#-*- coding: utf-8 -*-
# download data from pytdx (NOTES: using ticks to convert... not good yet)
# 20220213 Wanjo
# usage: python -i data_pytdx.py 000001.SZ 2021

from mypy import load,argv,argc,touch_dir,time_maker,tryx

config_o = load('../tmp/config.json')
assert config_o, 'Please check ../tmp/config.json'
#print(config_o)

root_data_path_1min = config_o[f'root_data_path_1min']
root_data_path_tick = config_o[f'root_data_path_tick']

from mytdx import mytdx
biz = mytdx(#'47.103.48.45'
              #'120.79.60.82',
              #'sztdx.gtjas.com',
              '139.159.143.228',#gz gbp
              7709)

import pandas as pd
    
def do_download(code, yyyy, i_save=0):
    tgt_dir = f'{root_data_path_1min}/tdx{yyyy}'
    tgt_fn = f'{tgt_dir}/{code}.parquet'
    
    print(f'downloading for {tgt_fn}...')

    df = tryx(lambda:biz.get_tick_yyyy(code, yyyy))

    if df is None: return
    
    df['tt'] = pd.DatetimeIndex((df[0]+8*3600*1000)*1000000)
    df.index = pd.to_datetime(df['tt'])
    df['p']=df[1]
    df['v']=df[2]
    df['w']=df[3]
    dft=df[['p','v','w']]
    dft.index = df.index
    
    if i_save>0:
        touch_dir(f'{root_data_path_tick}/tdx{yyyy}')
        dft.to_parquet(f'{root_data_path_tick}/tdx{yyyy}/{code}.parquet',compression='gzip')
    
    x = df.resample('1min')
    dfx=x.last()
    dfx = dfx.dropna(axis=0)
    dfx['close']=dfx[1]
    dfx['open']=df[1].resample('1min').first()
    dfx['high']=df[1].resample('1min').max()
    dfx['low']=df[1].resample('1min').min()
    dfx['volume']=100 * df[2].resample('1min').sum()
    dfx['amount']=100 * (df[2]*df[1]).resample('1min').sum()

    dfx.index=1000* dfx['tt'].map(lambda v:time_maker(date=v,infmt='%Y-%m-%d %H:%M:%S', outfmt=''))
    #print(dfx)
    dfo = dfx[['open','high','low','close','volume','amount']]
    print(dfo)
    
    #dfo = dfo[~dfo.index.duplicated()]
    #print(len(dfo))
    
    touch_dir(tgt_dir)
    dfo.to_parquet(tgt_fn,compression='gzip')

    return tgt_fn

if __name__ == '__main__':
    code = argv[1]
    yyyy = argv[2]
    i_save = int(argv[3]) if argc>3 else 0 # save tick
    
    tgt_fn = do_download(code,yyyy,i_save)
    if tgt_fn:
        # check
        dfcheck = pd.read_parquet(tgt_fn)
        dfcheck['tt'] = pd.DatetimeIndex((dfcheck.index+8*3600000)*1000000)
        print(dfcheck)
    import os
    os._exit(0)
    
    # TODO load back and save len to audit.h5
