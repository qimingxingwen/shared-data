#-*- coding: utf-8 -*-
# 2022-03-25 Wanjo
# usage: python data_ft.py HK.000700 2021
# notes: 分 K 提供最近 2 年数据，日 K 及以上提供最近 10 年的数据。

from mypy import load,argv,argc,touch_dir,time_maker,os

config_o = load('../tmp/config.json')
assert config_o, 'Please check ../tmp/config.json'
#print(config_o)

freq = '1min'
root_data_path = config_o[f'root_data_path_{freq}']
futu_host = config_o[f'futu_host']

code = argv[1]
yyyy = int(argv[2])

tgt_dir = f'{root_data_path}/ft{yyyy}'
tgt_fn = f'{tgt_dir}/{code}.parquet'
print(f'downloading for {tgt_fn}...')

from myfutu import futu_new
myfutu = futu_new(host=futu_host)

df,err = myfutu.pull(code,
    time_maker(date='{}-01-01'.format(yyyy)),
    time_maker(-1,date='{}-01-01'.format(yyyy+1)),
    )

touch_dir(tgt_dir)
df.to_parquet(tgt_fn)

#list(df.columns)
#['code', 'time_key', 'open', 'close', 'high', 'low', 'pe_ratio', 'turnover_rate', 'volume', 'turnover', 'change_rate', 'last_close']

print(myfutu.quota())
os._exit(0)
