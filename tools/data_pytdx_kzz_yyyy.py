# quick download from pytdx by year
# usage:
# python data_pytdx_agu_yyyy.py 2021


from mypy import load

from mytushare import biz
list_agu = biz.get_list_kzz()

from data_pytdx import do_download

#def do_download(code,yyyy):
#    print(code,yyyy)
#    import os
#    os.system(f'python data_pytdx.py {code} {yyyy}')
#    return 0

from mypy import parallel,load,argv
yyyy = argv[1]
parallel(lambda c:do_download(c,yyyy), list_agu, pool_size=3)

import os
os._exit(0)

